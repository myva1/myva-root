import { registerApplication, start } from "single-spa";

registerApplication({
  name: "@myva-mf/navbar",
  app: () => System.import("@myva-mf/navbar"),
  activeWhen: "/",
});

registerApplication({
  name: "@myva-mf/taskmanager",
  app: () => System.import("@myva-mf/taskmanager"),
  activeWhen: "/taskmanager",
});

registerApplication({
  name: "@myva-mf/home",
  app: () => System.import("@myva-mf/home"),
  activeWhen: "/home",
});

start();
